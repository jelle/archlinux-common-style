SASS ?= sass

all: css

.PHONY: sass-watcher
sass-watcher:
	@mkdir -p css
	$(SASS) --watch sass/archlinux.scss:css/archlinux.css sass/navbar.scss:css/navbar.css

.PHONY: css
css: archlinux.css navbar.css

%.css:
	@mkdir -p css
	$(SASS) -t compressed sass/$(subst .css,.scss,$@) css/$@

.PHONY:
clean:
	rm -rf css
